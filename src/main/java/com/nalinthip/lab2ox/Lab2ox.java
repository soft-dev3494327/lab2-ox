/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.nalinthip.lab2ox;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Lab2ox {

    static char[][] board = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
    private static char player = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        printStart();

        while (true) {
            printBoard();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printBoard();
                printWin();
                printContinue();
                break;
            }
            if (isDraw()) {
                printBoard();
                printDraw();
                printContinue();
                break;
            }
            switchPlayer();
        }
        inputContinue();
    }

    private static void printWelcome() {
        System.out.println(" ______________________________");
        System.out.println("|                              |");
        System.out.println("|      Welcome to XO Game      |");
        System.out.println("|______________________________|");
    }

    private static void printBoard() {
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + "|");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println("player " + player + " turn");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Please input number[1-3] in row/col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board[row - 1][col - 1] == '_') {
                board[row - 1][col - 1] = player;
                break;
            } else {
                System.out.println("Please input try again!!!");
            }
        }
    }

    private static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    private static boolean isWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(" ____________________________________ ");
        System.out.println("|                                    |");
        System.out.println("|  Congratulations! Player " + player + " wins!!! |");
        System.out.println("|____________________________________|");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[row - 1][i] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[i][col - 1] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (board[0][2] == player && board[1][1] == player && board[2][0] == player) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '_') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println(" ____________________");
        System.out.println("|                    |");
        System.out.println("|        Draw!       |");
        System.out.println("|____________________|");
    }

    private static void printStart() {
        Scanner sc = new Scanner(System.in);
        String start;
        System.out.print("Start Game Now? (y/n) : ");
        start = sc.nextLine().toLowerCase();
        while (!start.equals("n") && !start.equals("y")) {
            System.out.print("Please Try Again (y/n): ");
            start = sc.nextLine().toLowerCase();
        }
        if (start.equals("n")) {
            System.out.println(" ____________________");
            System.out.println("|                    |");
            System.out.println("|     Exit Game      |");
            System.out.println("|      ByeBye!!      |");
            System.out.println("|____________________|");
            System.exit(0);
        }
    }

    private static void inputContinue() {
        Scanner sc = new Scanner(System.in);
        String newgame = sc.next();
        if (newgame.equals("y")) {
            resetBoard();
            main(null);
        } else {
            System.out.println(" ____________________");
            System.out.println("|                    |");
            System.out.println("|     Exit Game      |");
            System.out.println("|      ByeBye!!      |");
            System.out.println("|____________________|");
            System.exit(0);
        }
    }

    private static void printContinue() {
        System.out.print("Would you like to play again? (y/n) : ");
    }
    
    private static void resetBoard() {
        board = new char[][]{{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
        player = 'X';
    }
}
